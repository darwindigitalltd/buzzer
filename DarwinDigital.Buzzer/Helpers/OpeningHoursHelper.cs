﻿using DarwinDigital.Buzzer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DarwinDigital.Buzzer.Helpers
{
    public class OpeningHoursHelper
    {
        private readonly ApplicationUser _user;

        public OpeningHoursHelper(ApplicationUser user)
        {
            _user = user;
        }
        public static bool IsCompanyOpen(ApplicationUser user)
        {
            var openingHoursJson = JsonConvert.DeserializeObject<List<OpeningHoursModel>>(user.OpeningHours);
            var currentDay = DateTime.Now;

            //gets the current day of the week from 0-6, monday being 0, sunday being 6
            var dayOfWeekNumber = (int)(currentDay.DayOfWeek + 6) % 7;

            var openingConfig = openingHoursJson[dayOfWeekNumber];

            var isOpen = false;
            if (!openingConfig.Closed)
            {
                //add the hours and minutes
                var openingHourTotal = (60 * openingConfig.OpenHour) + openingConfig.OpenMinute;
                var closingHourTotal = (60 * openingConfig.CloseHour) + openingConfig.CloseMinute;

                var currentHourTotal = (60 * currentDay.Hour) + currentDay.Minute;

                return currentHourTotal >= openingHourTotal && currentHourTotal <= closingHourTotal;
            }

            return false;
        }
    }
}
