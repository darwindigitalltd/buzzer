﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DarwinDigital.Buzzer.Helpers
{
    public static class PhoneNumberFormatHelper
    {
        public static string FormatPhoneNumber(string phoneNumber, string countryCode = "GB")
        {
            var formattedPhoneNumber = phoneNumber;
            switch(countryCode)
            {
                case "GB":
                    if(phoneNumber.Substring(0, 1) == "0")
                    {
                        formattedPhoneNumber = "+44" + phoneNumber.Substring(1, phoneNumber.Length-1);
                    }
                    return formattedPhoneNumber;
            }
            return phoneNumber;
        }

        public static bool IsPhoneNumberValid(string phoneNumber, string countryCode = "")
        {
            //UK country code regex
            var UKPattern = @"(\+44)[0-9]{10}";
            var r = new Regex(UKPattern, RegexOptions.IgnoreCase);
            return (Regex.Matches(phoneNumber, UKPattern).Count > 0);
        }
    }
}
