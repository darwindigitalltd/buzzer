﻿using DarwinDigital.Buzzer.Helpers;
using DarwinDigital.Buzzer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Threading.Tasks;
using DarwinDigital.Buzzer.Data;
using DarwinDigital.Buzzer.Models;
using System.Web;

namespace DarwinDigital.Buzzer.Controllers
{

    public class CallController
    {
        private readonly TwilioConfiguration _config;
        private readonly ApplicationDbContext _context;
        public CallController(IOptions<TwilioConfiguration> config, ApplicationDbContext context)
        {
            _config = config.Value;
            _context = context;
        }

        [HttpPost]
        [Route("Call/{clientId}/MakeCall/{customerPhone}")]
        public string MakeCall(string clientId, string customerPhone)
        {
            customerPhone = HttpUtility.UrlDecode(customerPhone);
            
            if (!_context.Users.Where(c => c.Id == clientId).Any())
            {
                return JsonConvert.SerializeObject(new { error = "No client exists", success = false }); ;
            }
            ApplicationUser currentCompany = _context.Users.Where(c => c.Id == clientId).FirstOrDefault();
            if(!OpeningHoursHelper.IsCompanyOpen(currentCompany))
            {
                var errorJson = new { status = "ERROR", message = "Call placed outside of client opening hours." };
                return JsonConvert.SerializeObject(errorJson);
            }

            var cleanCustomerPhone = PhoneNumberFormatHelper.FormatPhoneNumber(customerPhone);
            var clientPhone = currentCompany.PhoneNumberForCallback;

            if (!PhoneNumberFormatHelper.IsPhoneNumberValid(cleanCustomerPhone))
            {
                var errorJson = new { status = "number-invalid", message = "Invalid phone number" };
                return JsonConvert.SerializeObject(errorJson);
            }

            TwilioClient.Init(_config.AccountSID, _config.AuthToken);
            var to = new PhoneNumber(clientPhone);
            var from = new PhoneNumber(_config.OutboundPhoneNumber);
            var call = CallResource.Create(to, from,
               url: new Uri("https://handler.twilio.com/twiml/EHf08b6233b8c50c3807232bfb19a4d422?customerPhoneNumber=" + cleanCustomerPhone));
            var json = new { staus = call.Status, sid = call.Sid };
            return JsonConvert.SerializeObject(json);
        }

        [HttpGet]
        [Route("Call/{clientId}/FetchCall/{callSid}")]
        public string FetchCall(string clientId, string callSid)
        {
            if (!_context.Users.Where(c => c.Id == clientId).Any())
            {
                return JsonConvert.SerializeObject(new { error = "No client exists", success = false }); ;
            }

            TwilioClient.Init(_config.AccountSID, _config.AuthToken);
            var callInProgress = CallResource.Fetch(pathSid: callSid);
            return JsonConvert.SerializeObject(callInProgress);

        }
    }
}
