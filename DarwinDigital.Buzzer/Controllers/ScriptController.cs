﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using DarwinDigital.Buzzer.Data;
using DarwinDigital.Buzzer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DarwinDigital.Buzzer.Helpers;

namespace DarwinDigital.Buzzer.Controllers
{
    [Route("Script/{clientId}/ringring.js")]
    public class ScriptController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ScriptController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index(string clientId)
        {
            var account = _context.Users.Find(clientId);
            if (account == null)
            {
                return PartialView("ClientScript/_ScriptError");
            }
            if(!OpeningHoursHelper.IsCompanyOpen(account))
            {
                return PartialView("ClientScript/_OpeningHoursError");
            }
            /*
            if (System.IO.File.Exists("wwwroot/assets/js/dist/" + clientId + "/clientScript.min.js"))
            {
                var file = System.IO.File.ReadAllText("wwwroot/assets/js/dist/" + clientId + "/clientScript.min.js");
                return Content(file, "text/javascript");
            }*/

            if (System.IO.File.Exists("wwwroot/assets/js/dist/clientScript.js"))
            {
                var file = System.IO.File.ReadAllText("wwwroot/assets/js/dist/clientScript.js");
                return Content(file, "text/javascript");
            }

            return PartialView("ClientScript/_ScriptError");
        }

    }
}