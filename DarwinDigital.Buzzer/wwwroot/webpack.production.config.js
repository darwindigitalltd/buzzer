const webpack = require('webpack'),
    path = require('path'),
    WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    entry: {
        'app': [
            'babel-polyfill',
            './assets/vendor/jscolor-2.0.5/jscolor.js',
            './assets/js/react/index.jsx',
            './assets/js/modules/index.js'
        ],
        'clientScript': [
            './assets/js/clientScript/index.js'
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            output: {
                comments: false
            },
            sourceMap: false,
            minimize: true
        })
    ],
    output: {
        filename: '[name].min.js',
        publicPath: './assets/js/dist/',
        path: __dirname + '/assets/js/dist'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};
