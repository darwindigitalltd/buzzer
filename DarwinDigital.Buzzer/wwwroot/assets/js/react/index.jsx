import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import SignUpForm from "./SignUpForm/SignUpForm";
import OpeningHours from "./OpeningHours/OpeningHours";

const getRenderFunc = app => {
    return () => {
        render(app.component, app.element, app.name);
    };
};

const render = (Component, element, name) => {
    ReactDOM.render(
        <AppContainer name={name}>
            {Component}
        </AppContainer>,
        element
    );
};

let apps = [];

if (document.getElementById('sign-up__react-root')) {
    const el = document.getElementById('sign-up__react-root');
    apps.push(
        {
            element: el,
            component: <SignUpForm antiForgery={el.getAttribute('data-antiforgery')}/>,
            file: './SignUp/SignUp',
            name: 'SignUp'
        }
    );
}
if (document.getElementById('opening-hours__react-root')) {
    const el = document.getElementById('opening-hours__react-root');
    apps.push(
        {
            element: el,
            component: <OpeningHours initialData={el.getAttribute('data-initialData')} />,
            file: './OpeningHours/OpeningHours',
            name: 'OpeningHours'
        }
    );
}

for (let a of apps) {
    a.element ? getRenderFunc(a)() : null;
    module.hot ? module.hot.accept(getRenderFunc(a)()) : null;
}
