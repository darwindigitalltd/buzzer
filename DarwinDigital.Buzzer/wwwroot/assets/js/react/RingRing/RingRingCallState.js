const stauses = {
    unplaced: 'unplaced',
    ringing: 'ringing',
    busy: 'busy',
    pending: 'pending',
    inProgress: 'in-progress',
    completed: 'completed'
};
export default Object.freeze(stauses);
