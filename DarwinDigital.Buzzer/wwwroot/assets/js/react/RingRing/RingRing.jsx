import React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';
import RingRingForm from "./RingRingForm";
import {cache} from 'locache';

export default class RingRing extends React.Component {

    constructor(props) {
        super();
        this.state = {...props, tooltipVisible: false, formVisible: false};
        this.bound_bodyListener = this.bodyListener.bind(this);

    }

    closePopup() {
        cache('ringring__tooltipHidden').set('true');
        this.setState({
            tooltipVisible: false
        });
    }

    componentDidMount() {
        //return;
        setTimeout(() => {
            if (this.state.formVisible || cache('ringring__tooltipHidden').get() === 'true') {
                return;
            }
            this.setState({
                tooltipVisible: true
            });
        }, 1200);
    }

    bodyListener(e) {
        let currentElement = e.target;
        while(currentElement.parentElement !== null) {
            if(currentElement.classList.contains('ringring__react-root')) {
                return;
            }
            currentElement = currentElement.parentElement;
        }
        this.setState({formVisible: false});
        document.body.removeEventListener('click', this.bound_bodyListener);
    }

    handleRealButtonClick(e) {
        if (e.target.getAttribute('data-realbutton') === 'true') {

            document.body.addEventListener('click', this.bound_bodyListener);
            this.setState({tooltipVisible: false, formVisible: true});
        }
    }

    render() {
        return (
            <div className='ringring'>
                <RingRingForm visible={this.state.formVisible} clientId={this.props.clientId}
                              backgroundColor={this.props.backgroundColor}/>
                <div style={{backgroundColor: '#' + this.props.backgroundColor}}
                     className={css(styles.ringring__button)}
                     data-realbutton={'true'}
                     onClick={this.handleRealButtonClick.bind(this)}>
                    <div
                        className={css(this.state.tooltipVisible ? [styles.ringring__tooltip, styles.ringring__tooltip__visible] : styles.ringring__tooltip)}>
                        <span className={css(styles.ringring__tooltip__text)}>Click here to receive an immediate callback!</span>
                        <div onClick={this.closePopup.bind(this)}
                             className={css(styles.ringring__tooltip__closebutton)}></div>
                    </div>
                </div>
            </div>
        );
    }
}

const opacityKeyframes = {
    from: {
        opacity: 0
    },
    to: {
        opacity: 1
    }
};


const styles = StyleSheet.create({
    ringring__button: {
        padding: '8px 12px',
        backgroundColor: '#f00',
        width: 'auto',
        display: 'inline-block',
        borderRadius: '6px',
        cursor: 'pointer',
        position: 'relative',
        ':before': {
            color: '#fff',
            content: '\'\\f095\'',
            fontFamily: 'FontAwesome',
        },
    },
    ringring__tooltip__text: {
        opacity: 0.7,
        fontFamily: 'Gibson',
    },
    ringring__tooltip: {
        display: 'none',
        fontWeight: 300,
        lineHeight: 1.2,
        position: 'absolute',
        transform: 'translateY(-180%)',
        width: '155px',
        left: 0,
        textAlign: 'left',
        borderRadius: 5,
        padding: '15px 22px 15px 15px',
        backgroundColor: '#ededed',
        //border: '1px solid #999',

        ':after': {
            content: '\'\'',
            width: 0,
            height: 0,
            borderLeft: '10px solid transparent',
            borderRight: '10px solid transparent',
            borderTop: '10px solid #ededed',
            position: 'absolute',
            bottom: -9,
            left: 7
        }
    },
    ringring__tooltip__closebutton: {
        position: 'absolute',
        top: '5px',
        right: '5px',
        height: '10px',
        width: '10px',
        borderRadius: '5px',
        ':before': {
            content: '\'\\f00d\'',
            position: 'absolute',
            fontFamily: 'FontAwesome',
            left: '50%',
            top: '50%',
            color: '#444',
            transform: 'translate(-50%, -50%)'
        }
    },
    ringring__tooltip__visible: {
        display: 'block',
        animationName: [opacityKeyframes],
        animationDuration: '2s'
    }
});
