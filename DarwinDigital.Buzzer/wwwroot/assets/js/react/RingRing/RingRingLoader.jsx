import React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';
import RingRingCallState from "./RingRingCallState";

export default class RingRingLoader extends React.Component {

    constructor(props) {
        super();
        const loaderBars = (
            <div>
                <div className={css(styles.ringringloader__loadingBar)}></div>
                <div className={css(styles.ringringloader__loadingBar)}></div>
                <div className={css(styles.ringringloader__loadingBar)}></div>
                <div className={css(styles.ringringloader__loadingBar)}></div>
            </div>
        );
        const loaderError = (
            <div>
                <div className={css(styles.ringringloader__error)}></div>
            </div>
        );
        const loaderSuccess = (
            <div>
                <div className={css(styles.ringringloader__tick)}></div>
            </div>
        );
        this.state = {...props, loadComplete: false, error: false, loaderBars: loaderBars, loaderContent: loaderBars, loaderError: loaderError, loaderSuccess: loaderSuccess};
    }

    componentWillReceiveProps(newProps) {
        let callState = "",
            error = false,
            loadComplete = false,
            loaderContent = this.state.loaderBars;

        switch (newProps.callState) {
            case RingRingCallState.unplaced:
                callState = 'being placed';
                break;
            case RingRingCallState.ringing:
                callState = 'connecting';
                break;
            case RingRingCallState.inProgress:
                callState = 'in progress';
                break;
            case RingRingCallState.completed:
                callState = 'complete! Thank you.';
                loadComplete = true;
                loaderContent = this.state.loaderSuccess;
                break;
            case RingRingCallState.busy:
                callState = 'busy';
                error = true;
                loaderContent = this.state.loaderError;
                break;
            default:
                callState = 'being placed';
        }

        this.setState({
            callState: callState,
            error: error,
            loaderContent: loaderContent,
            loadComplete: loadComplete
        });
    }

    render() {
        return (

            <div className={css(styles.ringringloader)}>
                {this.state.loaderContent}
                <div
                    className={css(styles.ringringloader__text)}>
                    {this.state.error ? 'There was an error placing your call. Please reload the page and try again' : 'Your call is ' + this.state.callState}
                </div>
            </div>
        )
            ;
    }
}

const loadingKeyframes = {

    '0%': {
        transform: 'scale(1)'
    },
    '20%': {
        transform: 'scale(1, 2.2)'
    },
    '40%': {
        transform: 'scale(1)',
    }
};

const styles = StyleSheet.create({
    ringringloader: {
        padding: '20px 0'
    },
    ringringloader__error: {
        ':after': {
            content: '\'\\f06a\'',
            fontFamily: 'FontAwesome',
            fontSize: '2rem'
        }
    },
    ringringloader__tick: {
        ':after': {
            content: '\'\\f00c\'',
            fontFamily: 'FontAwesome',
            fontSize: '2rem'
        }
    },
    ringringloader__text: {
        marginTop: 20,
        fontFamily: 'Gibson',
        fontSize: '0.9rem'
    },
    ringringloader__loadingBar: {
        display: 'inline-block',
        width: '4px',
        height: '18px',
        borderRadius: '4px',
        marginRight: 5,
        animation: 'loading 1s ease-in-out infinite',
        animationName: loadingKeyframes,

        ':nth-child(1)': {
            backgroundColor: '#888'
        },
        ':nth-child(2)': {
            backgroundColor: '#888',
            animationDelay: '0.09s'
        },
        ':nth-child(3)': {
            backgroundColor: '#888',
            animationDelay: '0.18s'
        },
        ':nth-child(4)': {
            backgroundColor: '#888',
            animationDelay: '0.27s',
            marginRight: 0
        }
    }
});
