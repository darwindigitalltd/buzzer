import React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';
import request from 'superagent';
import RingRingLoader from "./RingRingLoader";
import RingRingCallState from './RingRingCallState';

export default class RingRingForm extends React.Component {

    constructor(props) {
        super();
        this.state = {...props, phoneValue: '', callState: RingRingCallState.unplaced};
        this.callUrl = 'https://localhost:3000' //'https://buzzer.darwindigital.co';
    }

    beginCallback(e) {
        e.preventDefault();

        const sanitisedPhone = encodeURIComponent(this.state.phoneValue);

        this.setState({
            callState: RingRingCallState.pending
        });

        request
            .post(this.callUrl + '/Call/' + this.props.clientId + '/MakeCall/' + this.state.phoneValue)
            .end((err, res) => {
                // Calling the end function will send the request
                const result = JSON.parse(res.text);
                this.getCallStatus(result.sid);
                this.startCallMonitoring(result.sid);
            });
    }

    startCallMonitoring(sid) {
        if (this.state.callState !== RingRingCallState.busy && this.state.callState !== RingRingCallState.completed) {
            setTimeout(this.getCallStatus(sid).bind(this), 5000)
        }
    }

    getCallStatus(sid = null) {
        if (sid === null) {
            return () => {
            };
        }
        return () => {
            request
                .get(this.callUrl + /Call/ + this.props.clientId + '/FetchCall/' + sid)
                .end((err, res) => {
                    const result = JSON.parse(res.text);
                    let callState = RingRingCallState.unplaced;

                    console.log(result.status);

                    switch (result.status) {
                        case 'busy' :
                            callState = RingRingCallState.busy;
                            break;
                        case 'ringing':
                            callState = RingRingCallState.ringing;
                            break;
                        case 'completed':
                            callState = RingRingCallState.completed;
                            break;
                        case 'in-progress':
                            callState = RingRingCallState.inProgress;
                            break;
                        default:
                            callState = RingRingCallState.unplaced;
                    }
                    this.setState({callState: callState});

                    this.startCallMonitoring(sid);
                });
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    render() {

        let content = this.state.callState === RingRingCallState.unplaced ? (<form onSubmit={this.beginCallback.bind(this)}>
            <div className={css(styles.ringringform__formRow)}>
                <label htmlFor={'ringringform__number'} className={css(styles.ringringform__label)}>Your
                    phone number</label>
                <input type={'text'} className={css(styles.ringringform__input)}
                       value={this.state.phoneValue}
                       onChange={e => this.setState({phoneValue: e.target.value})}/>
            </div>
            <button style={{backgroundColor: '#' + this.props.backgroundColor}} type={'submit'}
                    className={css(styles.ringringform__button)}>Request callback
            </button>
        </form>) : <RingRingLoader callState={this.state.callState} />;

        return this.state.visible ? (
            <div className={css(styles.ringringform)}>
                <div className={css(styles.ringringform__headerContainer)}>
                    <div className={css(styles.ringringform__headerContainer__inner)}>
                        <span className={css(styles.ringringform__header)}>Request a callback</span>
                    </div>
                    <div onClick={() => this.setState({visible: false})}
                         className={css(styles.ringringform__closeButton)}>
                    </div>
                </div>
                <div style={{textAlign: (this.state.callState === RingRingCallState.unplaced) ? 'left' : 'center'}}
                     className={css(styles.ringringform__contentContainer)}>
                    {content}
                </div>
            </div>
        ) : null;
    }
}

const styles = StyleSheet.create({
        ringringform: {
            background: '#ededed',
            width: 200,
            borderRadius: '5px',
            marginBottom: 20,
            position: 'relative',

            ':after': {
                content: '\'\'',
                width: 0,
                height: 0,
                borderLeft: '10px solid transparent',
                borderRight: '10px solid transparent',
                borderTop: '10px solid #ededed',
                position: 'absolute',
                bottom: -9,
                left: 7
            }
        },
        ringringform__closeButton: {
            float: 'right',
            marginRight: 10,
            cursor: 'pointer',
            borderTopRightRadius: 5,
            ':after': {
                content: '\'\\f00d\'',
                fontFamily: 'FontAwesome',
                color: '#fff'
            }
        },
        ringringform__formRow: {
            marginBottom: '15px'
        },
        ringringform__button: {
            border: 0,
            color: '#fff',
            padding: '8px 20px',
            cursor: 'pointer',
            background: '#79e2a6',
            borderRadius: 3,
            fontFamily: 'Gibson'
        },
        ringringform__input: {
            fontFamily: 'Gibson',
            padding: '7px 3px',
            border: 0,
            textIndent: 10,
            borderRadius: 3,
            boxSizing:
                'border-box'
        },
        ringringform__label: {
            display: 'block',
            fontSize: '0.9rem',
            marginBottom: 4,
            fontFamily: 'Gibson'
        },
        ringringform__headerContainer__inner: {
            padding:
                '0 10px',
            display: 'inline-block'
        },
        ringringform__headerContainer: {
            background: '#888',
            lineHeight: '30px',
            height: 30,
            borderTopRightRadius:
                '8px',
            borderTopLeftRadius:
                '8px'
        },
        ringringform__contentContainer: {
            padding: [12, 15]
        },
        ringringform__header: {
            display: 'block',
            color: '#fff',
            fontFamily: 'Gibson'
        }
    })
;
