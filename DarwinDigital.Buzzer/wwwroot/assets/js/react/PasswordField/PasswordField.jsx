import React from 'react';

export default class PasswordField extends React.Component {

    constructor(props) {
        super();
        this.state = {...props, passwordHidden: true};
    }

    mouseDown() {
        this.setState({
            passwordHidden: false
        });
    }
    mouseUp() {
        this.setState({
            passwordHidden: true
        });
    }

    render() {
        return (
            <div>
                <label className={this.props.labelClassName} htmlFor={this.props.id}>{this.props.label}</label>
                <div className={'password-field__input-container'}>
                    <input type={this.state.passwordHidden ? 'password' : 'text'} name={this.props.name} id={this.props.id}
                           className={this.props.className}/>
                    <div
                        className={'password-field__show-button' + (this.state.passwordHidden ? ' --hidden' : ' --shown')}
                        onMouseDown={this.mouseDown.bind(this)} onMouseUp={this.mouseUp.bind(this)} onMouseLeave={this.mouseUp.bind(this)}></div>
                </div>
            </div>
        );
    }
}
