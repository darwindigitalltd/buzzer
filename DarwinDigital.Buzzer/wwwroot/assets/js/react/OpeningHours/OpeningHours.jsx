import React from 'react';

export default class OpeningHours extends React.Component {

    constructor(props) {
        super();
        let hourOptions = [];
        const defaultTimeSelections = [
            {day: 'Monday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: false},
            {day: 'Tuesday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: false},
            {day: 'Wednesday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: false},
            {day: 'Thursday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: false},
            {day: 'Friday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: false},
            {day: 'Saturday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: true},
            {day: 'Sunday', openHour: '09', openMinute: '00', closeHour: '17', closeMinute: '00', closed: true},
        ];
        let dataToUse = defaultTimeSelections;
        try {
            const initialData = JSON.parse(props.initialData);
            dataToUse = initialData;
        }
        catch (e) {
            console.warn('error parsing JSON - the default set of values has been used');
        }

        this.state = {
            ...props,
            timeSelections: dataToUse
        };
    }

    generateOptions(isSelectForHours) {
        const counter = isSelectForHours ? 24 : 60;
        let options = [];
        for (let i = 0; i < counter; (isSelectForHours ? i++ : i += 15)) {
            const paddedCount = i.toString().padStart(2, '0');
            options.push(<option
                key={(isSelectForHours ? ('hours_counter' + i) : ('minutes_counter' + i))}>{paddedCount}</option>);
        }
        return options;
    }

    handleOpeningChange(day) {
        return (e) => {
            let timeSelections = this.state.timeSelections;
            timeSelections[day].closed = e.target.checked;
            this.setState({
                timeSelections: timeSelections
            });
        }
    }

    updateOpeningHours(day, isHours, isOpening) {
        return (e) => {
            let timeValue = e.target.value,
                openingHoursDay = this.state.timeSelections[day],
                timeSelections = this.state.timeSelections;

            if (isHours) {
                if (isOpening) {
                    openingHoursDay.openHour = timeValue;
                }
                else {
                    openingHoursDay.closeHour = timeValue;
                }
            }
            else {
                if (isOpening) {
                    openingHoursDay.openMinute = timeValue;
                }
                else {
                    openingHoursDay.closeMinute = timeValue
                }
            }
            timeSelections[day] = openingHoursDay;
            this.setState({
                timeSelections: timeSelections
            });
        }
    }

    render() {
        let fieldsets = [], counter = 0;
        for (let day of this.state.timeSelections) {
            fieldsets.push(
                <fieldset className={'opening-hours__fieldset'} key={day.day}>
                    <legend className={'opening-hours__legend'}>{day.day}</legend>
                    <label htmlFor={day.day + '__closed-bool'} className={'opening-hours__closed-bool-label'}>Are you
                        closed
                        on {day.day}</label>
                    <input type={'checkbox'} defaultChecked={this.state.timeSelections[counter].closed}
                           id={day.day + '__closed-bool'} name={day.day + '__closed-bool'} value={'true'}
                           onChange={this.handleOpeningChange(counter).bind(this)}/>
                    <div style={{display: (day.closed ? 'none' : 'block')}}>
                        <div className={'opening-hours__day-container'}>
                            <span className={'opening-hours__time-event'}>Opening Time</span>
                            <label htmlFor={day.day + '__open-hour-select'} className={'hidden-label'}>Opening time
                                hour</label>
                            <select className={'opening-hours__select --hours'} id={day.day + '__open-hour-select'}
                                    onChange={this.updateOpeningHours(counter, true, true)} defaultValue={day.openHour}>
                                {this.generateOptions(true)}
                            </select>
                            <span>:</span>
                            <label htmlFor={day.day + '__open-minute-select'} className={'hidden-label'}>Opening time
                                minute</label>
                            <select className={'opening-hours__select --hours'} id={day.day + '__open-minute-select'}
                                    onChange={this.updateOpeningHours(counter, false, true)}
                                    defaultValue={day.openMinute}>
                                {this.generateOptions(false)}
                            </select>
                        </div>
                        <div className={'opening-hours__day-container'}>
                            <span className={'opening-hours__time-event'}>Closing Time</span>
                            <label htmlFor={day.day + '__open-hour-select'} className={'hidden-label'}>Opening time
                                hour</label>
                            <select className={'opening-hours__select --hours'} id={day.day + '__close-hour-select'}
                                    defaultValue={day.closeHour}
                                    onChange={this.updateOpeningHours(counter, true, false)}>
                                {this.generateOptions(true)}
                            </select>
                            <span>:</span>
                            <label htmlFor={day.day + '__open-minute-select'} className={'hidden-label'}>Opening time
                                minute</label>
                            <select className={'opening-hours__select --hours'} id={day.day + '__close-minute-select'}
                                    defaultValue={day.closeMinute}
                                    onChange={this.updateOpeningHours(counter, false, false)}>
                                {this.generateOptions(false)}
                            </select>
                        </div>
                    </div>
                </fieldset>
            );
            counter++;
        }

        return (
            <div className={'opening-hours'}>
                {fieldsets}
                <input type={'hidden'} name={'OpeningHours'} value={JSON.stringify(this.state.timeSelections)}/>
            </div>
        );
    }
}
