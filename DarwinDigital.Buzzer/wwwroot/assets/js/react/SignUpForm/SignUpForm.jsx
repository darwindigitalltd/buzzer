import React from 'react';
import PasswordField from '../PasswordField/PasswordField';

export default class SignUpForm extends React.Component {

    constructor(props) {
        super();
        this.state = {...props, clicked: 'false'};
    }

    clicked() {
        this.setState({
            clicked: 'clicked'
        });
    }

    render() {
        return (
            <div className={'sign-up-form'}>
                <form method="post" action="/Users/Create"  className="sign-up-form__form">
                    <div className="sign-up-form__row">
                        <label htmlFor="sign-up-form__name" className="sign-up-form__label">Full name</label>
                        <input name="Name" type="text" id={'sign-up-form__name'} className={'sign-up-form__input input --text'}/>
                    </div>
                    <div className="sign-up-form__row">
                        <label htmlFor="sign-up-form__email" className="sign-up-form__label">Email address</label>
                        <input name="Email" type="email" id={'sign-up-form__email'} className={'sign-up-form__input input --email'}/>
                    </div>
                    <div className="sign-up-form__row">
                        <PasswordField label={'Password'} labelClassName={'sign-up-form__label'} name={'Password'} id={'sign-up-form__password'} className={'sign-up-form__input input --password'} />
                    </div>

                    <input type={'hidden'} name={'__RequestVerificationToken'} value={this.props.antiForgery} />
                    
                    <div className="sign-up-form__row">
                        <button className="sign-up-form__submit button --submit">Sign up</button>
                    </div>
                </form>
            </div>
        );
    }
}
