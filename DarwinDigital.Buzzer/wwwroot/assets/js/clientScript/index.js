if (!global._babelPolyfill) {
    require('babel-polyfill');
}

import React from 'react';
import ReactDOM from 'react-dom';
import RingRing from '../react/RingRing/RingRing';
import parseQueryString from './parseQueryString/parseQueryString';

let button = document.createElement('div');
button.classList.add('ringring__react-root');
button.style.position = 'fixed';
button.style.bottom = '20px';
button.style.left = '20px';

let fontAwesome = document.createElement('link');
fontAwesome.setAttribute('href', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css');
fontAwesome.setAttribute('integrity', 'sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt');
fontAwesome.setAttribute('crossorigin', 'anonymous');


let gibson = document.createElement('style');
gibson.innerHTML = '@font-face {\n' +
    '    font-family: Gibson;\n' +
    '    src: url(\'https://localhost:3000/assets/fonts/Gibson-Regular.otf\');\n' +
    '}';




document.addEventListener('DOMContentLoaded', function() {
    document.body.appendChild(button);
    document.head.appendChild(fontAwesome);
    document.head.appendChild(gibson);
    ReactDOM.render(<RingRing backgroundColor={ringring_backgroundColor || '4d1daa'} clientId={ringring_clientId} />, button);
});
