import SignUpForm from "./assets/js/react/SignUpForm/SignUpForm";

const twilio = require('twilio');
const express = require('express');
const path = require('path');
const csp = require('helmet-csp');
const app = express();
const PNF = require('google-libphonenumber').PhoneNumberFormat;
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const webpack = require('webpack');
const webpackConfig = require('./webpack.config');
const compiler = webpack(webpackConfig);
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import {addNewUser} from "./logic/users/addNewUser";
const sql = require('mssql');

const accountSid = 'AC175ce6dec907bbb2800abe70ceea0337';
const authToken = '3fe7538e415b6bb5feeb76480f088413';
let client = new twilio(accountSid, authToken);

app.set('view engine', 'pug');
app.use(csp({
  directives: {
    scriptSrc: ["'self'", "'unsafe-inline'"],
  }
}));
app.use('/assets', express.static(path.join(__dirname, 'assets')));
app.use(express.urlencoded());

app.get('/client/:id/script', (req, res) => {
  res.send(`var button = document.createElement("button");
   button.style.height = '30px';
   button.style.width = '30px';
   button.style.position = 'fixed';
   button.style.cursor='pointer';
   button.style.bottom = '20px';
   button.style.left = '20px';
   button.style.borderRadius = '10px';
   button.style.background = 'url("http://www.iconsplace.com/download/white-phone-512.png") #f00d31 no-repeat center';
   button.style.backgroundSize = '15px';
   button.style.border = 'none';
   button.style.padding = '15px';
   button.style.color = '#fff';
   document.addEventListener('DOMContentLoaded', function() {
   document.body.appendChild(button); });`);
});

app.get('/', (req, res) => {
  res.render('index', {title: 'Ring Ring Derivery', myDivContents: 'welcome to the site'});
});

const signUpForm = ReactDOMServer.renderToString(<SignUpForm />);
app.get('/sign-up', (req, res) => {
  res.render('sign-up', {title: 'Ring Ring', signUpForm: signUpForm, pageTitle: 'Sign up'});
});

app.post('/sign-up', (req, res) => {
   //perform checks to add new user
   addNewUser(req.body['sign-up-form__name'], req.body['sign-up-form__email'], req.body['sign-up-form__password'], req.body['sign-up-form__phone']).then(() => {
       res.render('sign-up-complete', {success: true});
   }).catch(() => {
       res.render('sign-up-complete', {success: false});
   });
});

app.get('/client/:id/call/:number', (req, res) => {
  let number = req.params.number;

  number = phoneUtil.parseAndKeepRawInput(number, 'GB');
  console.log(phoneUtil.format(number, PNF.E164));

  /*client.calls.create({
      url: 'http://developmentlab.net/call.xml',
      to: '+447807809417',
      method: 'get',
      from: '+441515416900',
  }).then(() => console.log('successful call made')).catch(err => console.log(err));*/
});

app.listen(999, () => {
  console.log('listening on 999');
});
