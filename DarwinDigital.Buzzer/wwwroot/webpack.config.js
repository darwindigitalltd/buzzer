const webpack = require('webpack'),
    path = require('path'),
    WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    entry: {
        'bloc': [
            'babel-polyfill',
            /*'react-hot-loader/patch',
            'webpack-hot-middleware/client',*/
            './assets/vendor/jscolor-2.0.5/jscolor.js',
            './assets/js/react/index.jsx',
            './assets/js/modules/index.js'
        ],
        'clientScript': [
            './assets/js/clientScript/index.js'
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new WriteFilePlugin({test: /^bloc.js$/, force: true})
    ],
    output: {
        filename: '[name].js',
        path: __dirname + '/assets/js/dist'
    },
    module: {
        rules: [

            {
                enforce: "pre",
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'eslint-loader'
            },

            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};
