﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DarwinDigital.Buzzer.Services
{
    public class SiteUrlConfiguration
    {
        public string SiteUrl { get; set; }
    }
}
