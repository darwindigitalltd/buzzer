﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DarwinDigital.Buzzer.Services
{
    public class TwilioConfiguration
    {
        public string AccountSID { get; set; }
        public string AuthToken { get; set; }
        public string OutboundPhoneNumber { get; set; }
    }
}
