﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DarwinDigital.Buzzer.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string ButtonBackgroundColour { get; set; }
        public string PhoneNumberForCallback { get; set; }
        public string OpeningHours { get; set; }

    }
}
