﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DarwinDigital.Buzzer.Models.ManageViewModels
{
    public class ChangeSettingsViewModel
    {
        [Display(Name = "Button Background Colour")]
        public string ButtonBackgroundColour { get; set; }

        [Display(Name = "Telephone number to call when user clicks callback button")]
        public string PhoneNumberForCallback { get; set; }
        public string OpeningHours { get; set; }
        public string StatusMessage { get; set; }
    }
}
