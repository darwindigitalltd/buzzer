﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DarwinDigital.Buzzer.Models
{
    public class OpeningHoursModel
    {
        [JsonProperty("day")]
        public string Day { get; set; }
        [JsonProperty("openHour")]
        public int OpenHour { get; set; }
        [JsonProperty("openMinute")]
        public int OpenMinute { get; set; }
        [JsonProperty("closeHour")]
        public int CloseHour { get; set; }
        [JsonProperty("closeMinute")]
        public int CloseMinute { get; set; }
        [JsonProperty("closed")]
        public bool Closed { get; set; }
    }
}
