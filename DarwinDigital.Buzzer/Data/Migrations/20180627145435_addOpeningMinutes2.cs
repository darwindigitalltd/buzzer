﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DarwinDigital.Buzzer.Data.Migrations
{
    public partial class addOpeningMinutes2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteWednesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteWednesday",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClosingMinuteFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteWednesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteWednesday",
                table: "AspNetUsers");
        }
    }
}
