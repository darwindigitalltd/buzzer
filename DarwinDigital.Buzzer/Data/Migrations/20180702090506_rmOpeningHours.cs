﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DarwinDigital.Buzzer.Data.Migrations
{
    public partial class rmOpeningHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClosingHourFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourWednesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingMinuteWednesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourWednesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningMinuteTuesday",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "OpeningMinuteWednesday",
                table: "AspNetUsers",
                newName: "OpeningHours");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OpeningHours",
                table: "AspNetUsers",
                newName: "OpeningMinuteWednesday");

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourWednesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingMinuteWednesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourWednesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningMinuteTuesday",
                table: "AspNetUsers",
                nullable: true);
        }
    }
}
