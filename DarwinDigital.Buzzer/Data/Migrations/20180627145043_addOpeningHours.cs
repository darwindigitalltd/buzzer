﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DarwinDigital.Buzzer.Data.Migrations
{
    public partial class addOpeningHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClosingHourFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingHourWednesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourFriday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourMonday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourSaturday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourSunday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourThursday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourTuesday",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHourWednesday",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClosingHourFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClosingHourWednesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourFriday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourMonday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourSaturday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourSunday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourThursday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourTuesday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OpeningHourWednesday",
                table: "AspNetUsers");
        }
    }
}
