﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DarwinDigital.Buzzer.Data.Migrations
{
    public partial class AddBgColour3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BackgroundColour",
                table: "AspNetUsers",
                newName: "ButtonBackgroundColour");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ButtonBackgroundColour",
                table: "AspNetUsers",
                newName: "BackgroundColour");
        }
    }
}
