using DarwinDigital.Buzzer.Helpers;
using System;
using Xunit;

namespace Buzzer.Tests
{
    public class PhoneNumberFormatHelperTest
    {
        [Theory]
        [InlineData("+4478078094")]
        [InlineData("+4478")]
        [InlineData("")]
        public void InvalidPhoneNumberShouldReturnFalse(string value)
        {
            var isValid = PhoneNumberFormatHelper.IsPhoneNumberValid(value);
            Assert.False(isValid);
        }

        [Theory]
        [InlineData("+447807809417")]
        [InlineData("+447807809416")]
        [InlineData("+447807809999")]
        public void ValidPhoneNumberReturnsTrue(string value)
        {
            var isValid = PhoneNumberFormatHelper.IsPhoneNumberValid(value);
            Assert.True(isValid);
        }
    }
}
